### Install certificate on IOS device

- **Download both certificates (red and black)**
![image](https://https://gitlab.com/hci261125/hci-documentation/-/blob/main/Install%20certificate/Iphone%20pics/level5.jpeg?inline=false)


- **Go to Files and search  for the certificates**

- **Choose one of them and than choose your Iphone device**

- **Go to your device settings, "Profile Downloaded" and install the certificate**


**Now do the whole process again with the seconed certificate**
