### Install certificate on windows

```הורידו את הקבצים הבאים```

[HCIrootCA.red.crt](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Install%20certificate/HCIrootCA.red.crt?inline=false)

[HCIrootCA.black.crt](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Install%20certificate/HCIrootCA.black.crt?inline=false)

```לחצו פעמיים על קובץ התעודה ולחצו על פתח -> התקנת אישור```

![image](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Install%20certificate/sd.png?inline=false)

```לאחר מכן יש לאשר עד קבלת האישור הבא: ```

![image](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Install%20certificate/b.png?inline=false)

```בהגדרות דפדפן כרום, חפשו "ניהול אישורי מכשירים"```

![image](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Install%20certificate/c.png?inline=false)

```בחרו ברשויות אישורים עליונות מהימנות ולחצו על ייבוא```

![image](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Install%20certificate/d.png?inline=false)

```תצטרכו לבחור את קובץ התעודה ולאשר את ההגדרות הדיפולטיות עד שמתקבל האישור הבא: ```

![image](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Install%20certificate/e.png?inline=false)


**וזהו! מזל טוב, התקנתם את התעודות של HCI**
**זכרו לעבור על התהליך גם עבור HCIrootCA.red.crt וגם עבור HCIrootCA.black.crt**
