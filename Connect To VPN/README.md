### Download and install OpenVPN Connect v3 on your PC

[OpenVPN Connect Download](https://openvpn.net/downloads/openvpn-connect-v3-windows.msi)

### Import your profile
```download this profile:```
[VPN-SECURE.ovpn](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Connect%20To%20VPN/VPN-SECURE.ovpn?inline=false)

```import it with your OpenVPN Connect software```
![image](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Connect%20To%20VPN/vpn-connect.png?inline=false)


### Enter Username And Password
```enter your username and password values:```
![image](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Connect%20To%20VPN/vpn-connect-2.png?inline=false)

**NOTE: if your values are not authorized, please contact your מפקד מגמה to fix it on the RED DOMAIN CONTROLLER**

```click connect and....```


### Your IN !!!!
![image](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Connect%20To%20VPN/vpn-connect-3.png?inline=false)

**Enjoy !!!!**
