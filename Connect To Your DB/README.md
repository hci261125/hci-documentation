### Port fowarding your cluster
```first, you need to install oc client and connect to your project:```

[oc-4.12.22-windows.zip](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Connect%20To%20Your%20DB/oc-4.12.22-windows.zip?inline=false)

https://docs.openshift.com/container-platform/4.8/cli_reference/openshift_cli/getting-started-cli.html

**run the following 2 commands for port fowarding your DB:**
**don't forget to change the NAMESPACE and CLUSTER_NAME**

```
oc get pod -n NAMESPACE -o name -l postgres-operator.crunchydata.com/cluster=CLUSTER_NAME,postgres-operator.crunchydata.com/role=master
```

**get the result from the 1st command and paste it in the RESULT variable:**

```
oc -n NAMESPACE port-forward RESULT 5432:5432
```

### Connect via pgadmin 4



**go to your deployment secrets and look for your db connection values, it will be something like** ```db-test-pguser-db-test```

![image](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Connect%20To%20Your%20DB/screencapture-console-apps-openshift-black-hci-k8s-ns-test-secrets-yahav-test-pguser-yahav-test-2023-06-23-10_01_49.png?inline=false)

**now, connect to pgadmin 4 with that values**

![image](https://gitlab.com/hci261125/hci-documentation/-/raw/main/Connect%20To%20Your%20DB/image.png?inline=false)

**And your in, ENJOY !!!**
